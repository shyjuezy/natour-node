const mongoose = require('mongoose')
const slugify = require('slugify')
const validator = require('validator')

const tourSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'A tour must have a name'],
        unique: true,
        trim: true,
        maxlength: [40, 'A tour name cannot exceeds 40 character'],
        minlength: [10, 'A tour name must have a minimum of 10 characters']
        // validate: [validator.isAlpha, 'Tour name should be characters']
    },
    slug: String,
    duration: {
        type: Number,
        required: true
    },
    maxGroupSize: {
        type: Number,
        required: [true, 'A tour must have a group size']
    },
    difficulty: {
        type: String,
        required: [true, 'A tour must have a difficulty level'],
        enum: {
            values: ['difficult', 'easy', 'medium'],
            message: 'Difficultly is either easy, medium or difficult'
        }
    },
    ratingsAverage: {
        type: Number,
        default: 4.5,
        min: [1, 'Rating must be greater than 0'],
        max: [1, 'Rating cannot exceeds 5']
    },
    ratingsQuantity: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        required: [true, 'A tour must have a price']
    },
    priceDiscount: {
        type: Number,
        // This validator function will only works for new documents, it will not work on updates(put/patch)
        validate: {
            validator: function (val) {
                return val < this.price
            },
            message: `Discount price({VALUE}) should be below regular price`
        }
    },
    summary: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true
    },
    imageCover: {
        type: String,
        required: [true, 'A tour must have a cover image']
    },
    images: [String],
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false
    },
    startDates: [Date],
    secretTour: {
        type: Boolean,
        default: false
    }
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})

tourSchema.virtual('durationWeeks').get(function () {
    return this.duration / 7
})

// Mongoose Document middleware that runs before .save() and .create()
// tourSchema.pre('save', function (next) {
//     this.slug = slugify(this.name, { lower: true })
//     next()
// })

// tourSchema.pre('save', function (next) {
//     console.log('Will save documents....')
//     next()
// })

// tourSchema.post('save', function (doc, next) {
//     console.log(doc)
//     next()
// })

// Mongoose Query Middleware
tourSchema.pre(/^find/, function (next) {
    this.find({ secretTour: { $ne: true } })
    this.start = Date.now()
    next()
})

tourSchema.post(/^find/, function (doc, next) {
    console.log(`Query took ${Date.now() - this.start} millisecond`)
    // console.log(doc)
    next()
})

// Mongoose Aggregate Middleware
tourSchema.pre('aggregate', function (next) {
    this.pipeline().unshift({ $match: { secretTour: { $ne: true } } })
    next()
})

const Tour = mongoose.model('Tour', tourSchema)

module.exports = Tour