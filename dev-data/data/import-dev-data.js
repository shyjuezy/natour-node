const dotenv = require('dotenv')
dotenv.config({ path: './config.env' })
const Tour = require('./../../models/tourModel')
const mongoose = require('mongoose')
const fs = require('fs')

const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DB_PASSWORD)

mongoose.connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}).then(() => console.log('DB Connection was successful'))
    .catch(err => {
        console.log('Could not connect to DB', err)
    })

console.log(process.argv)

const tours = JSON.parse(fs.readFileSync(`${__dirname}/tours-simple.json`, 'utf-8'))

const importData = async () => {
    console.log('Calling importData', tours)
    try {
        await Tour.create(tours)
        console.log('Data successfully loaded')
    } catch (err) {
        console.log(err)
    }
}

const deleteAllData = async () => {
    console.log('Calling deleteAllData')
    try {
        const data = await Tour.deleteMany()
        console.log('Data successfully deleted')
    } catch (err) {
        console.log(err)
    }
}

if (process.argv[2] === '--import') {
    importData()
} else if (process.argv[2] === '--delete') {
    deleteAllData()
}

// process.exit()
