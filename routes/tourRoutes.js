const express = require('express')
const { getAllTours, createTour, getTour, updateTour, deleteTour, aliasTopTours, getTourStats, getMonthlyPlan } = require('../controllers/tourController')

const router = express.Router()

// express param middleware
// router.param('id', checkID)
// router.use(checkBody)

// 1) ROUTES
router.route('/top-5-tours').get(aliasTopTours, getAllTours)
router.route('/monthly-plan/:year').get(getMonthlyPlan)
router.route('/tour-stats').get(getTourStats)
router.route('/').get(getAllTours).post(createTour)
router.route('/:id').get(getTour).patch(updateTour).delete(deleteTour)

module.exports = router